from django.db import IntegrityError
from django.test import TransactionTestCase, RequestFactory

from menus.management.commands.mock_menu import mock_for
from menus.models import Menu, Item
from menus.templatetags.draw_menu import compile_tree
from menus.views import MainView


class DrawMenuTest(TransactionTestCase):
    def setUp(self):
        mock_for(self)
        self.factory = RequestFactory()

    def tearDown(self):
        Item.objects.all().delete()
        Menu.objects.all().delete()

    def test_models(self):
        with self.assertRaises(IntegrityError):
            another_menu_a = Menu(title="simple_menu").save()

        # with self.assertRaises(IntegrityError):
        #     Item(title="A", parent=None, menu=self.menu_a).save()

    def test_compile_tree(self):
        menu_items = Item.objects.filter(menu__title="simple_menu")
        tree = compile_tree(menu_items)

        self.assertEquals(tree[self.home.pk]['item'], self.home)
        self.assertEquals(
            tree[self.home.pk]['child'][self.store.pk]['item'], self.store
        )

    def test_html(self):
        need = """
<!DOCTYPE html>
<html>
    <head>
        <title>Динамическое меню</title>
    </head>
<body>
<ul><li><a href='/home/'>home</a><ul><li><a href='/home/store/'>store</a><ul><li><a href='/home/store/cart/'>cart</a><ul></ul></li><li><a href='/home/store/storefront/'>storefront</a></li><li><a href='/home/store/support/'>support</a></li></ul></li><li><a href='/home/help/'>help</a></li></ul></li></ul>
</body>
</html>
"""
        request = self.factory.get("/home/store/cart/")
        response = MainView.as_view()(request)
        response.render()

        _content = str(response.content, "utf-8")
        self.assertEqual(_content, need)
