from django.core.management import BaseCommand

from menus.models import Item, Menu


def mock_for(obj):
    obj.menu_a = Menu(title="simple_menu")
    obj.menu_a.save()

    obj.home = Item(title="home", parent=None, menu=obj.menu_a)
    obj.home.save()

    obj.store = Item(title="store", parent=obj.home, menu=obj.menu_a)
    obj.store.save()

    obj.cart = Item(title="cart", parent=obj.store, menu=obj.menu_a)
    obj.cart.save()
    obj.storefront = Item(title="storefront", parent=obj.store, menu=obj.menu_a)
    obj.storefront.save()
    obj.support = Item(title="support", parent=obj.store, menu=obj.menu_a)
    obj.support.save()

    obj.help = Item(title="help", parent=obj.home, menu=obj.menu_a)
    obj.help.save()

    obj.about = Item(title="about", parent=obj.help, menu=obj.menu_a)
    obj.about.save()
    obj.contacts = Item(title="contacts", parent=obj.help, menu=obj.menu_a)
    obj.contacts.save()
    obj.faq = Item(title="faq", parent=obj.help, menu=obj.menu_a)
    obj.faq.save()


class Command(BaseCommand):
    def handle(self, *args, **options):
        mock_for(self)
