from django.db.models import ForeignKey, CharField
from django.db.models import Model


class Menu(Model):
    title = CharField(max_length=128, unique=True)


class Item(Model):
    parent = ForeignKey('self', null=True)
    title = CharField(max_length=128, null=False)
    menu = ForeignKey(Menu, null=False)

    class Meta:
        unique_together = (('title', 'parent'),)
