from django import template
from django.utils.safestring import mark_safe

from menus.models import Item

register = template.Library()


def compile_tree(items):
    _tree = {}
    _items = list(items)

    def attach_item_to_tree(in_item, in_tree):
        if not in_item.parent:
            in_tree[in_item.pk] = {"item": in_item, "child": {}}
            return True
        else:
            _parent = in_tree.get(in_item.parent.pk, None)
            if _parent:
                _parent["child"][in_item.pk] = {"item": in_item, "child": {}}
                return True
            else:
                _recursion_result = False

                for _key, _value in in_tree.items():
                    _subtree = _value["child"]
                    _recursion_result |= attach_item_to_tree(in_item, _subtree)

                return _recursion_result

    while len(_items):
        _item = _items.pop(0)
        _attached = attach_item_to_tree(_item, _tree)
        if not _attached:
            _items.append(_item)

    return _tree


def mark_path(in_tree, in_path):
    """
    Recursively mark nodes in tree if node is opened (menu item is opened)
    :param in_tree: dict-like obj {'item': <item>, 'child': <subtree>}
    :param in_path: list of bread crumbs
    :return: None
    """
    if not in_tree or not in_path:
        return

    _current_crumb = in_path.pop(0)

    for _key, _value in in_tree.items():
        if _value["item"].title == _current_crumb:
            _value["open"] = True
            _subtree = _value["child"]
            mark_path(_subtree, list(in_path))


def draw_tree(in_tree, in_breadcrumb):
    """
    Recursively draw tree nodes
    :param in_tree: dict-like obj
        {'item': <item>, 'child': <subtree>, ['open': True]}
    :param in_breadcrumb: list of bread crumbs
    :return: html node
    """
    _items = []
    _node_template = "<li>{title}{submenu}</li>"
    _menu_template = "<ul>{nodes}</ul>"
    _link_template = "<a href='/{path}/'>{text}</a>"

    for _key, _value in in_tree.items():
        _item = _value["item"]

        _breadcrumb = list(in_breadcrumb)
        _breadcrumb.append(_item.title)
        _item_path = "/".join(_breadcrumb)

        _title = _link_template.format(path=_item_path, text=_item.title)

        _submenu = ""
        if _value.get("open"):
            _subtree = _value["child"]
            _submenu = draw_tree(_subtree, list(_breadcrumb))

        _items.append(
            _node_template.format(title=_title, submenu=_submenu)
        )
    _nodes = "".join(_items)

    return _menu_template.format(nodes=_nodes)


@register.simple_tag(takes_context=True)
def draw_menu(context, name):
    """
    Usage: {% draw_menu 'some_menu_name' %}

    :param context: django tag context
    :param name: menu name
    :return: html node
    """
    _, __, _data, *___ = context
    _request = _data["request"]
    _path = _request.path
    _breadcrumb = list(filter(lambda x: x, _path.split("/")))
    _menu_items = list(Item.objects.filter(menu__title=name))
    _tree = compile_tree(_menu_items)
    mark_path(_tree, list(_breadcrumb))
    _html = draw_tree(_tree, [])
    return mark_safe(_html)
