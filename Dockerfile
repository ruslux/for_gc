FROM python:3.5

COPY ./requirements.txt /var/www/requirements.txt
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install -r /var/www/requirements.txt

ENV PYTHONPATH=/usr/lib/python3/dist-packages:/var/www LANG=ru_RU.UTF-8 DJANGO_SETTINGS_MODULE=config.settings